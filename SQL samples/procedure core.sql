declare @a�o int;
declare @individual bit;
declare @tipoGenero char;

set @a�o = 1999;
set @individual = null;
set @tipoGenero = 'T';


select 
	a.id_pais
	, d.genero
	, oro =    case when   m.nombre = 'oro' then 1 else 0 end
	, plata =  case when   m.nombre = 'plata' then 1 else 0 end
	, bronce = case when   m.nombre = 'bronce' then 1 else 0 end
	, total_medallas = count(*) over (partition by d.genero)
	, total_plata = count(case when m.nombre = 'plata' then 1 else null end) over (partition by d.genero)
from 
	dbo.PxDxJJOO a
	join dbo.disciplinas_JJOO d  on d.id_disciplina = a.id_disciplina	
	join medallas m  on a.id_medalla = m.id_medalla
where 
	d.a�o = @a�o
	and ( @individual is null or @individual = d.individual)
	and ( @tipoGenero = 'T' or @tipoGenero = d.genero)



