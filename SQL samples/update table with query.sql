
alter table cajas_ahorros
add saldo_cuenta decimal(9,2) not null default '0';
go

update
    ca
set
    ca.saldo_cuenta = c.saldo
from
    (select * from cajas_ahorros) as ca

    join 

	(select ca.nro_cliente, 
			ca.nro_cuenta, 
			sum(case when tm.debito_credito = 'C' then mov.importe_movimiento else -mov.importe_movimiento end) as saldo
	from clientes c
		join cajas_ahorros ca on ca.nro_cliente = c.nro_cliente
		join mov_cajas_ahorros mov on mov.nro_cuenta = ca.nro_cuenta
		join tipos_movimientos tm on tm.cod_tipo_movimiento = mov.cod_tipo_movimiento
	group by ca.nro_cuenta, ca.nro_cliente) as c

    on ca.nro_cliente = c.nro_cliente


select * from cajas_ahorros

