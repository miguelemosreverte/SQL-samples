
--create database examen_final;
use examen_final;
go

go
drop table calificaciones
go
drop table competencias
go
drop table PxDxJJOO
go
drop table participantes
go
drop table competidores_equipos
go
drop table equipos
go
drop table medallas
go
drop table disciplinas_JJOO
go
drop table disciplinas
go
drop table competidores
go
drop table JJOO
go
drop table ciudades
go
drop table paises
go


--EXEC sp_fkeys 'equipos'--encontrar quien esta referenciando tabla

create table paises(
	id_pais int not null primary key,
	nombre varchar(200) not null
)

create table ciudades(
	id_pais int not null,
	id_ciudad int not null,
	nombre varchar(200) not null,
	primary key(id_pais, id_ciudad)
)


create table JJOO(
	a�o int not null,
	id_pais int not null,
	id_ciudad int not null,
	primary key(a�o),
	foreign key(id_pais, id_ciudad) references ciudades
)

create table disciplinas(
	id_disciplina int not null,
	nombre varchar(200) not null,
	primary key(id_disciplina)
)


create table disciplinas_JJOO(
	id_disciplina int not null references disciplinas,
	a�o int not null references JJOO,
	primary key(id_disciplina, a�o),

	fecha_inicio date not null,
	fecha_fin date not null,
	genero char null check (genero in ('F','M','A')),
	individual bit not null
	
)


create table medallas(
	id_medalla int not null primary key,
	nombre varchar(200) not null
)
create table equipos(
	id_equipo int not null,
	primary key (id_equipo)
)

create table competidores(
	nro_doc int not null,
	fecha_nacimiento date not null,
	sexo char not null check (sexo in ('F','M')),
	nombre varchar(200) not null,
	apellido varchar(200) not null,
	primary key(nro_doc),
)

create table competidores_equipos(
	nro_doc int not null references competidores,
	id_equipo int not null references equipos,
	primary key(nro_doc, id_equipo)
)

create table participantes(
	
	id_pais int not null references paises,	
	a�o int not null references JJOO,
	id_participante int not null,
	nro_doc int null references competidores,
	id_equipo int null references equipos,
	primary key(id_participante, a�o, id_pais),
	check ((nro_doc is null and id_equipo is not null) 
	or (nro_doc is not null and id_equipo is null))
)

create table PxDxJJOO(
	id_pais int not null,	
	a�o int not null,
	id_participante int not null,
	id_disciplina int not null,
	
	foreign key(id_participante, a�o, id_pais) references participantes,
	foreign key(id_disciplina, a�o) references disciplinas_JJOO,
	primary key(id_participante, a�o, id_pais, id_disciplina),
	id_medalla int null references medallas,
)
create table competencias(
	id_competencia int not null,
	a�o int not null,
	id_disciplina int not null,
	primary key(id_competencia, a�o, id_disciplina),
	foreign key (a�o, id_disciplina) references disciplinas_JJOO,
	
	fecha_hora date not null
)


create table calificaciones(
	id_competencia int not null,
	a�o int not null,
	id_disciplina int not null,
	foreign key (id_competencia, a�o, id_disciplina) references competencias,

	id_pais int not null,
	foreign key (id_pais, a�o, id_disciplina) references participantes,
	
	id_calificacion int not null,
	primary key(id_competencia, a�o, id_disciplina, id_pais, id_calificacion),

	calificacion int null
)
go

insert into paises(id_pais, nombre)
values(0,'pais0')
go
insert into ciudades(id_ciudad,id_pais,nombre)
values(0,0,'ciudad0')
go

insert into competidores(nro_doc,nombre,apellido,fecha_nacimiento,sexo)
values(0,'nombre0', 'apellido0', '05-05-1999', 'M'),
	  (1,'nombre1', 'apellido1', '05-05-1999', 'M'),
	  (2,'nombre2', 'apellido2', '05-05-1999', 'M'),
	  (3,'nombre3', 'apellido3', '05-05-1999', 'M'),
	  (4,'nombre4', 'apellido4', '05-05-1999', 'M')
go

insert into disciplinas(id_disciplina, nombre)
values(0, 'disciplina0'),
	  (1, 'disciplina1'),
	  (2, 'disciplina2'),
	  (3, 'disciplina3'),
	  (4, 'disciplina4')
go

insert into JJOO(a�o,id_ciudad,id_pais)
values(1999, 0, 0)
go
insert into disciplinas_JJOO(id_disciplina, a�o, fecha_inicio, fecha_fin, genero, individual)
values(0, 1999, '05-05-1999', '05-05-1999', 'A', 0),
	  (1, 1999, '05-05-1999', '05-05-1999', 'M', 1),
	  (2, 1999, '05-05-1999', '05-05-1999', 'M', 0),
	  (3, 1999, '05-05-1999', '05-05-1999', 'M', 1),
	  (4, 1999, '05-05-1999', '05-05-1999', 'M', 0)
go

insert into medallas(id_medalla, nombre)
values(0, 'oro'),
	(1, 'plata'),
	(2, 'bronce')
go

insert into equipos(id_equipo)
values(0)
go


insert into competidores_equipos(id_equipo,nro_doc)
values(0,1),
	(0,2)
go



insert into participantes(a�o,id_pais,id_participante,id_equipo,nro_doc)
values(1999,0,0,0,null),
	(1999,0,1,null,1),
	(1999,0,2,null,2),
	(1999,0,3,null,3),
	(1999,0,4,null,4)
go

insert into PxDxJJOO(a�o, id_disciplina,id_medalla,id_pais,id_participante)
values(1999,0,0,0,0),
	  (1999,1,1,0,1),
	  (1999,2,2,0,2),
	  (1999,3,2,0,3),
	  (1999,4,1,0,4)
go






go

/*
join participantes p on p.a�o = i.a�o and p.id_pais = i.id_pais and p.id_participante = i.id_participante
			join competidores_equipos e on p.id_equipo = e.id_equipo
			join competidores c on e.nro_doc = c.nro_doc
			join disciplinas d on d.id_disciplina = i.id_disciplina
			join tipo_di*/

--crear todos los triggers que impiden cambio de clave primaria para las tablas que afecten el resultado
/*create trigger tu_ri_inscriptos
on competidores_equipos
for update
as 
begin
	if update() or update(a�o) or update(id_disciplina) or update(id_pais) or update(id_calificacion)
	begin

		raiserror('No es permitido modificar las claves primarias',16,1)
		rollback
	end
end*/
go

create trigger tiu_ri_inscripciones
on PxDxJJOO
for insert, update
as
begin
	--case 1: un solo competidor
	if exists(			
			select *	   
			from inserted i
			join participantes p on p.a�o = i.a�o and p.id_pais = i.id_pais and p.id_participante = i.id_participante
			join competidores c on p.nro_doc = c.nro_doc
			join disciplinas d on d.id_disciplina = i.id_disciplina
			join tipo_disciplina td on td.id_tipo_disciplina = d.id_tipo_disciplina
			where c.sexo <> td.sexo and td.sexo <> 'A'
	)
	begin
		raiserror('El sexo del inscripto es diferente del de la disciplina.',16,1)
		rollback
		return
	end

	--case 2: un equipo de competidores
	if exists(			
			select *	   
			from inserted i
			join participantes p on p.a�o = i.a�o and p.id_pais = i.id_pais and p.id_participante = i.id_participante
			join competidores_equipos e on p.id_equipo = e.id_equipo
			join competidores c on e.nro_doc = c.nro_doc
			join disciplinas d on d.id_disciplina = i.id_disciplina
			join tipo_disciplina td on td.id_tipo_disciplina = d.id_tipo_disciplina
			where c.sexo <> td.sexo and td.sexo <> 'A'
	)
	begin
		raiserror('El sexo del inscripto es diferente del de la disciplina.',16,1)
		rollback
		return
	end
end
go





drop procedure ejercicio3_final
go

create procedure ejercicio3_final
(
@a�o	integer,
@individual	bit,
@tipoGenero	char
)
as
begin
	 select	a.id_pais, a.id_disciplina, d.genero, a.id_medalla
	from 
	PxDxJJOO a
	join disciplinas_JJOO d on d.a�o = @a�o and d.id_disciplina = a.id_disciplina	
	and d.genero = @tipoGenero and d.individual = @individual

end

go




drop procedure ejercicio3_final_subqueries
go

create procedure ejercicio3_final_subqueries
(
@a�o	integer,
@individual	bit,
@tipoGenero	char
)
as
begin
	 select	a.id_pais, a.id_disciplina, d.genero, 
	 oro =    case when exists(    select a.id_medalla from PxDxJJOO a join medallas m on m.id_medalla = a.id_medalla and d.a�o = @a�o and d.id_disciplina = a.id_disciplina	where m.nombre = 'oro') then 1 else 0 end,
	 plata =  case when exists(    select a.id_medalla from PxDxJJOO a join medallas m on m.id_medalla = a.id_medalla and d.a�o = @a�o and d.id_disciplina = a.id_disciplina	where m.nombre = 'plata') then 1 else 0 end,
	 bronce = case when exists(    select a.id_medalla from PxDxJJOO a join medallas m on m.id_medalla = a.id_medalla and d.a�o = @a�o and d.id_disciplina = a.id_disciplina	where m.nombre = 'bronce') then 1 else 0 end
	from 
	PxDxJJOO a
	join disciplinas_JJOO d on d.a�o = @a�o and d.id_disciplina = a.id_disciplina	
	and d.genero = @tipoGenero and d.individual = @individual

end

go


exec ejercicio3_final @a�o = 1999, @individual = 1, @tipoGenero = 'M'
go

exec ejercicio3_final_subqueries @a�o = 1999, @individual = 1, @tipoGenero = 'M'
go
