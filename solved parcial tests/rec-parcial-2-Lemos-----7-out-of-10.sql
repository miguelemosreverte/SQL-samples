-- INGENIERIA EN INFORMATICA - SISTEMAS DE BASES DE DATOS - EVALUACI�N PARCIAL RECUPERATORIA N� 2 - 29-06-2017

drop table mov_cajas_ahorros
drop table tipos_movimientos
drop table cajas_ahorros
drop table tipos_cuentas
drop table clientes
go


create table clientes
(
 nro_cliente		integer			not null,
 apellido			varchar(40)		not null,
 nombre				varchar(40)		not null,
 nro_documento		integer			not null
 primary key (nro_cliente),
 unique (nro_documento)
)
go
insert into clientes values(1,'apellido1', 'nombre1','123'),(2,'apellido2', 'nombre2','1234')
go

create table tipos_cuentas
(
 cod_tipo_cuenta	varchar(3)		not null,
 desc_tipo_cuenta	varchar(30)		not null,
 cant_max_ext_mes	tinyint			not null,
 primary key (cod_tipo_cuenta)
)
go

insert into tipos_cuentas (cod_tipo_cuenta, desc_tipo_cuenta, cant_max_ext_mes)
values ('COM', 'Com�n',    3),
       ('ESP', 'Especial', 1)

create table cajas_ahorros
(
 nro_cliente		integer			not null, 
 nro_cuenta			integer			not null, 
 cod_tipo_cuenta	varchar(3)		not null,
 primary key (nro_cuenta),
 foreign key (nro_cliente) references clientes,
 foreign key (cod_tipo_cuenta) references tipos_cuentas
)
go

insert into cajas_ahorros (cod_tipo_cuenta, nro_cliente, nro_cuenta)
values ('COM', 1, 1),
       ('ESP', 1, 2),
       ('COM', 2, 3)

create table tipos_movimientos
(
 cod_tipo_movimiento	varchar(3)		not null,
 desc_tipo_movimiento	varchar(30)		not null,
 debito_credito			char(1)			not null,
 primary key (cod_tipo_movimiento),
 check (debito_credito in ('D','C'))
)
go

insert into tipos_movimientos (cod_tipo_movimiento, desc_tipo_movimiento, debito_credito)
values ('DEP', 'Dep�sito',                  'C'),
       ('EXT', 'Extracci�n',                'D'),
	   ('GTO', 'Gastos administrativos',    'D'),
	   ('ACI', 'Acreditaci�n de intereses', 'C')

create table mov_cajas_ahorros
(
 nro_cuenta				integer			not null,
 fecha_movimiento		date			not null,
 cod_tipo_movimiento	varchar(3)		not null,
 nro_movimiento			integer			not null,
 importe_movimiento		decimal(9,2)	not null,
 primary key (cod_tipo_movimiento, nro_movimiento),
 foreign key (nro_cuenta) references cajas_ahorros,
 foreign key (cod_tipo_movimiento) references tipos_movimientos
)
go

insert into mov_cajas_ahorros (nro_cuenta, fecha_movimiento, cod_tipo_movimiento, nro_movimiento, importe_movimiento)
values (1,'1999-02-02','DEP', 1, 1000),
	   (2,'1999-02-02','GTO', 3, 200),

	   (3,'1999-02-02','EXT', 4, 4000),

	   (2,'1999-02-02','EXT', 7, 1),
	   (2,'1999-03-02','EXT', 8, 1),
	   (2,'1999-03-02','EXT', 9, 1),
	   (2,'1999-03-02','EXT', 10, 1),
	   
       (1,'1999-02-02','EXT', 2, 5),
	   (1,'1999-02-02','EXT', 11, 1),	   
	   (1,'1999-02-02','EXT', 12, 1),
	   (1,'1999-02-02','EXT', 13, 1)



/*
RESOLVER LOS SIGUIENTES EJERCICIOS:

1. (35) Programar un procedimiento almacenado que reciba como argumentos: nro. de cliente y a�o y devuelva como resultado la
   lista de sus cuentas que cumplan con alguno de los siguientes criterios:
   
   a. Tienen un saldo negativo
   b. Tienen m�s extracciones en alg�n mes que las permitidas seg�n el tipo de cuenta
   
   
	
	 Los resultados se deber�n mostrar de la siguiente manera:
   ---------------------------------------------------------------------------------------------------------------------------
   nro_cuenta	desc_tipo_cuenta		     saldo	cant_max_ext_mes	mes_a�o		cant_ext_mes	observaciones
   ---------------------------------------------------------------------------------------------------------------------------
   xxxxxxxxxx	xxxxxxxxxxxxxxxxxxxxx	xxxxxxx.xx        xxx			xx/xxxx		    xxx			xxxxxxxxxxxxxxxxxxxxxxxxxx
   xxxxxxxxxx	xxxxxxxxxxxxxxxxxxxxx	xxxxxxx.xx        xxx			xx/xxxx		    xxx			xxxxxxxxxxxxxxxxxxxxxxxxxx
   xxxxxxxxxx	xxxxxxxxxxxxxxxxxxxxx	xxxxxxx.xx        xxx			xx/xxxx		    xxx			xxxxxxxxxxxxxxxxxxxxxxxxxx
   xxxxxxxxxx	xxxxxxxxxxxxxxxxxxxxx	xxxxxxx.xx        xxx			xx/xxxx		    xxx			xxxxxxxxxxxxxxxxxxxxxxxxxx

   Donde observaciones puede ser:
   a. Si el saldo es negativo: El saldo de la cuenta es negativo
   b. Si tiene m�s extracciones en alg�n mes que las permitidas: Tiene m�s extracciones que las permitidas

   Se deben mostrar ordenadas por nro_cuenta.
   
   NOTA: la cuenta aparecer� tantas veces como problemas se detecten. Es decir, si una cuenta tiene saldo negativo y en dos meses
   tiene m�s extracciones que las permitidas se mostrar�n 3 filas en el resultado.

   En las filas en que se muestra el problema del saldo, mes_a�o y cant_ext_mes deben ser nulos. En todas las filas se debe mostrar
   el saldo y la cant_max_ext_mes.

	
*/


drop function obtener_saldo_de_cuenta
go

create function obtener_saldo_de_cuenta
(
@nro_cuenta	integer
)
returns int
as
begin
	return(
	select sum(case when mov.cod_tipo_movimiento in ('DEP','ACI') then mov.importe_movimiento else -mov.importe_movimiento end)
	from clientes c
		join cajas_ahorros ca on ca.nro_cliente = c.nro_cliente
		join mov_cajas_ahorros mov on mov.nro_cuenta = ca.nro_cuenta
		where ca.nro_cuenta = @nro_cuenta
	group by ca.nro_cuenta, ca.nro_cliente
	)
end
go


drop view clientes_cuentas_on_saldo_negativo
go
create view clientes_cuentas_on_saldo_negativo(nro_cliente,nro_cuenta)
as
	select ca.nro_cliente, ca.nro_cuenta
	from clientes c
		join cajas_ahorros ca on ca.nro_cliente = c.nro_cliente
		join mov_cajas_ahorros mov on mov.nro_cuenta = ca.nro_cuenta
	group by ca.nro_cuenta, ca.nro_cliente
	having sum(case when mov.cod_tipo_movimiento in ('DEP','ACI') then mov.importe_movimiento else -mov.importe_movimiento end) < 0 --saldo negativo
go



drop procedure dbo.ejercicio1_rec
go

create procedure dbo.ejercicio1_rec
(
@nro_cliente	integer,
@a�o	integer
)
as
begin

	select	ci.nro_cuenta, 
			tc.desc_tipo_cuenta, 
			(select dbo.obtener_saldo_de_cuenta(ci.nro_cuenta)) as saldo,
			tc.cant_max_ext_mes, 
			ci.mes_a�o, 
			ci.cant_extracciones,
			ci.observacion
	from
	(
		select vista1.nro_cuenta, 
				NULL as mes_a�o, 
				NULL as cant_extracciones,
				'El saldo de la cuenta es negativo' as observacion
		from clientes_cuentas_on_saldo_negativo vista1
		where vista1.nro_cliente = @nro_cliente
	
		union all

		select mov.nro_cuenta, 
				right(convert(varchar,mov.fecha_movimiento,103),7) as mes_a�o, 
				count(*) as cant_extracciones,
				'Tiene m�s extracciones que las permitidas' as observacion
		from mov_cajas_ahorros mov
		join cajas_ahorros ca on ca.nro_cuenta = mov.nro_cuenta
		join tipos_cuentas tc on tc.cod_tipo_cuenta = ca.cod_tipo_cuenta
		where mov.cod_tipo_movimiento = 'EXT'
			  and year(mov.fecha_movimiento) = @a�o
			  and ca.nro_cliente = @nro_cliente
		group by mov.nro_cuenta, ca.nro_cliente, month(mov.fecha_movimiento),tc.cant_max_ext_mes, mov.fecha_movimiento
		having tc.cant_max_ext_mes < count(*)
	) ci --cuentas_que_infringen_normas
	join cajas_ahorros ca on ca.nro_cuenta = ci.nro_cuenta
	join tipos_cuentas tc on tc.cod_tipo_cuenta = ca.cod_tipo_cuenta
	order by nro_cuenta
end

go


exec ejercicio1_rec @nro_cliente = 1, @a�o = 1999
go

exec ejercicio1_rec @nro_cliente = 2, @a�o = 1999
go








/*
2. (10) Determinar los triggers necesarios para mantener la regla de integridad: "En cada caja de ahorros, la cantidad 
   de extracciones por mes no puede superar el m�ximo permitido para el tipo de cuenta.

3. (30) Programar los triggers determinados en el punto anterior.
*/


--					insert							delete								update
--
--mov_cajas_ahorros controlar						no controlar					controlar, y no permitir cambio de PK
--
--cajas_ahorros		no controlar					no controlar					no permitir cambio de PK





create trigger tu_ri_cuentas
on cajas_ahorros
for update
as
begin
	if UPDATE(nro_cuenta)
	begin
		raiserror('no se permite el cambio',16,1)
		rollback
		return
	end
end
go

create trigger tu_ri_mov_cajas_ahorros
on mov_cajas_ahorros
for update
as
begin
	if UPDATE(nro_movimiento) or UPDATE(cod_tipo_movimiento)
	begin
		raiserror('no se permite el cambio',16,1)
		rollback
		return
	end
end
go


create trigger ti_ri_mov_cajas_ahorros
on mov_cajas_ahorros
for insert
as
begin
	if exists(			
			select ca.nro_cliente	   
			from inserted i
			join mov_cajas_ahorros mov on mov.nro_cuenta = i.nro_cuenta
			join cajas_ahorros ca on ca.nro_cuenta = mov.nro_cuenta
			join tipos_cuentas tc on tc.cod_tipo_cuenta = ca.cod_tipo_cuenta
			where mov.cod_tipo_movimiento = 'EXT'
			group by mov.nro_cuenta, ca.nro_cliente, month(mov.fecha_movimiento),tc.cant_max_ext_mes
			having tc.cant_max_ext_mes < count(*)

	)
	begin
		raiserror('La cantidad de extracciones por mes no puede superar el m�ximo permitido para el tipo de cuenta.',16,1)
		rollback
		return
	end
end
go














/*
4.	Programe las siguientes actualizaciones:

    a. (5) Agregar la columna saldo_cuenta como decimal(9,2) obligatoria a la tabla cajas_ahorros.
	
	b. (20) Actualizar la columna saldo_cuenta sumando el importe_movivimiento si el tipo de movimiento es de cr�dito y restando en 
	   el caso de que sea de d�bito.

*/

alter table cajas_ahorros
add saldo_cuenta decimal(9,2) not null default '0';
go

update
    ca
set
    ca.saldo_cuenta = c.saldo
from
    (select * from cajas_ahorros) as ca

    join 

	(select ca.nro_cliente, 
			ca.nro_cuenta, 
			sum(case when tm.debito_credito = 'C' then mov.importe_movimiento else -mov.importe_movimiento end) as saldo
	from clientes c
		join cajas_ahorros ca on ca.nro_cliente = c.nro_cliente
		join mov_cajas_ahorros mov on mov.nro_cuenta = ca.nro_cuenta
		join tipos_movimientos tm on tm.cod_tipo_movimiento = mov.cod_tipo_movimiento
	group by ca.nro_cuenta, ca.nro_cliente) as c

    on ca.nro_cliente = c.nro_cliente


select * from cajas_ahorros

